# Notes sur XML

Des informations plus ou moins techniques à propos de [XML] sont rassemblées
dans [le wiki][wiki] sous forme d'articles unitaires. Leur extraction et leur
mise en page dans des documents PDF sont destinées à une consultation sans
connexion à Internet.

sélection                                | pdf
-----------------------------------------|------------------------------------
introduction théorique idéologique       | [theorie.pdf](https://frabad.keybase.pub/xml/notes/theorie.pdf)
termes et technologies associées         | [termes.pdf](https://frabad.keybase.pub/xml/notes/termes.pdf)
(!) la syntaxe en quelques minutes       | [syntaxe.pdf](https://frabad.keybase.pub/xml/notes/syntaxe.pdf)
(!) codage de caractères et de documents | [codage.pdf](https://frabad.keybase.pub/xml/notes/codage.pdf)
(!) opérations techniques courantes      | [operations.pdf](https://frabad.keybase.pub/xml/notes/operations.pdf)

**note** : Les documents marqués d'un point d'exclamation (!) nécessitent
davantage de connaissances informatiques.

## Meta

meta     | description
---------|------------
mot-clés | XML, DTD, XSD, XSLT, rédaction, validation, transformation, wysiwyg, wysiwym
sujet    | guide, introduction, synthèse, notes, cours, manuel
auteur   | [keybase.io/frabad][keybase]


[wiki]: https://gitlab.com/frabad/notes-xml/wikis/home
[snippets]: https://gitlab.com/frabad/notes-xml/snippets/

[XML]: https://fr.wikipedia.org/wiki/XML
[DITA]: https://fr.wikipedia.org/wiki/DITA
[Markdown]: https://fr.wikipedia.org/wiki/Markdown
[keybase]: https://keybase.io/frabad

