# Licence

![Licence Creative Commons][CCI]  
Cette œuvre est mise à disposition selon les termes de la Licence
[CC BY-SA 4.0][CCL] : *Creative Commons Attribution - Partage dans
les Mêmes Conditions 4.0 International*.

[CCI]: https://i.creativecommons.org/l/by-sa/4.0/80x15.png
[CCL]: https://creativecommons.org/licenses/by-sa/4.0/deed.fr
